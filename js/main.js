var perRow = 4;
var totalRows = 2;

function dataCtrl($scope) {
	$scope.csvSourceData = parseSourceData();
	$scope.csvSpellData = parseSpellData();
	$scope.sourceLoopLength = function() {
		var csvLength = $scope.csvSourceData.length/perRow;
		return loop(csvLength);
	};
	$scope.spellLoopLength = function() {
		var csvLength = $scope.csvSpellData.length/perRow;
		return loop(csvLength);
	};
	
	$scope.currentSourcePosition = 0;
	$scope.currentSpellPosition = 0;
	$scope.nextFourSources = function() {
		var entries = [];
		var i = 0;

		for(i; i < perRow; i++) {
			entries.push($scope.csvSourceData[$scope.currentSourcePosition + i]);
		}
		$scope.currentSourcePosition += perRow;
		return entries;
	}
	$scope.nextFourSpells = function() {
		var entries = [];
		var i = 0;

		for(i; i < perRow; i++) {
			entries.push($scope.csvSpellData[$scope.currentSpellPosition + i]);
		}
		$scope.currentSpellPosition += perRow;
		return entries;
	}
	$scope.printSourceSpace = function() {
		return $scope.currentSourcePosition % (perRow*totalRows) == 0;
	}
	$scope.printSpellSpace = function() {
		return $scope.currentSpellPosition % (perRow*totalRows) == 0;
	}
}
var loop = function (csvLength) {
	var i=0;
	var loops = [];
	for(i; i< csvLength; i++) {
		loops.push(i);
	}
	return loops;
}

var parseData = function (CSVText) {
	CSVText = CSVText.replace(/\+/g, ',');
	var allTextLines = CSVText.split(/\r\n|\n/);
	var i;
	var lines = [];
	
	for(i = 0; i < allTextLines.length; i++) {
		var lineEntries = allTextLines[i].split('|');

		var card = {
			name: lineEntries[0],
			colour: lineEntries[1],
			mana: lineEntries[2],
			shield: lineEntries[3],
			on_ability_cost: lineEntries[4],
			on_abilit_desc: lineEntries[5],
			other_ability_cost: lineEntries[7],
			other_ability_colour: lineEntries[6],
			other_ability: lineEntries[8]
		};
		lines.push(card);
	}
	return lines;
}

var parseSourceData = function () {
	var CSVText = CSVSources.replace(/,/g, '|');
	return parseData(CSVText);
}

var parseSpellData = function () {
	var CSVText = CSVSpells.replace(/,/g, '|');
	return parseData(CSVText);
}

var CSVSources = ',Orange,6,1,5,Switch the field color to any color.,Off Color,12,Reduce the opponents energy by 3.\r\n'
	CSVSources += ',Orange,5,0,4,Attack for 5 damage.,Off Color,4,Attack for 3 damage.\r\n'
	CSVSources += ',Orange,4,2,4,Attack for 2 damage. Reduce opponents energy by 2.,Orange,4,During your opponents next turn increase your shields by 2.\r\n'
	CSVSources += ',Orange,4,0,4,During your opponent\'s next turn+ add 2 to your shields.,n/a,n/a,n/a\r\n'
	CSVSources += ',Orange,4,3,8,Destroy target source. During your opponent\'s next turn+ reduce your shields to 0.,Off Color,6,Attack for 5 damage.\r\n'
	CSVSources += ',Orange,3,3,12,Draw a card.,Off Color,10,Draw a card.\r\n'
	CSVSources += ',Orange,3,1,1,Attack for 2 damage.,Off Color,18,Both players shuffle their hands into their decks and draw cards equal to the amount they discarded.\r\n'
	CSVSources += ',Orange,3,0,2,Attack for 3 damage.,Blue,12,Gain 5 knowledge+ gain 10 mana. You may only play this ability once per turn.\r\n'
	CSVSources += ',Orange,3,2,5,Gain 1 knowledge. Switch the field color to silver.,Silver,5,Gain 2 knowledge. Switch the field color to orange.\r\n'
	CSVSources += ',Orange,2,5,20,Decrease your opponents shields by 5.,Orange,6,Attack for 7 damage.\r\n'
	CSVSources += ',Orange,2,1,3,Attack your opponent for 2 damage+ reduce your opponents energy by 3.,n/a,n/a,n/a\r\n'
	CSVSources += ',Orange,2,2,2,Pay 10 life. Deal 5 damage to your opponent. You may play this ability only once per turn.,Silver,10,Detain target source. Attack for 3 damage.\r\n'
	CSVSources += ',Orange,2,1,20,Your opponent loses 20 energy+ plus 1/3 of the amount of energy you have remaining+ rounded up. Reduce your energy to 0.,Silver,20,Gain 13 knowledge.\r\n'
	CSVSources += ',Orange,2,2,0,Deal 1 damage to your opponent (ignore shields). You may play this ability only once per turn.,Off Color,6,Switch the field color to orange.\r\n'
	CSVSources += ',Orange,2,0,1,Attack for 2 damage.,Off Color,2,Switch the field color to purple.\r\n'
	CSVSources += ',Orange,1,0,8,Destroy target source. Your opponent may also destroy target source.,Silver,8,Destroy target source. Sacrafice this source.\r\n'
	CSVSources += ',Orange,1,1,1,Attack for 2 damage.,Orange,3,Attack for 5 damage.\r\n'
	CSVSources += ',Orange,1,1,6,Target source does not generate energy during its controllers next turn.,Purple,1,Generate 2 mana. You may play this ability only once per turn.\r\n'
	CSVSources += ',Orange,0,0,1,Switch the field color to any color except Orange.,Off Color,0,Switch the field color to Orange.\r\n'
	CSVSources += ',Orange,0,3,8,Attack for 10 damage.,Silver,10,Gain 6 knowledge.\r\n'
	CSVSources += ',Purple,6,0,5,Switch the field colour to any colour.,On Colour,12,Leech mana from opponent by 5\r\n'
	CSVSources += ',Purple,6,0,10,Apply ignore shield to other Source\'s next attack,Silver,10,Gain 5 knowledge.\r\n'
	CSVSources += ',Purple,5,0,5,Leech mana from opponent for 3.  Change field colour to orange.,Orange,10,Burn 5 opponent mana.  Change field colour to purple.\r\n'
	CSVSources += ',Purple,5,0,5,Gain 2 mana for each time your opponent switches field colours.,Silver,10,Increase shields by 3.\r\n'
	CSVSources += ',Purple,5,0,10,Draw a card.  Change field colour to anything but Purple,n/a,n/a,n/a\r\n'
	CSVSources += ',Purple,5,0,10,Ignore one opponent Source shield for the rest of the time it\'s in play.  You may not switch field colours for the rest of your turn.,Orange,12,Attack for 10\r\n'
	CSVSources += ',Purple,4,3,1,Switch field colour to any colour,n/a,n/a,n/a\r\n'
	CSVSources += ',Purple,4,0,5,Reduce opponent mana by 2.  You may only play this ability once per turn.,Silver,10,Gain 4 knowledge.  Change field colour to anything but silver.\r\n'
	CSVSources += ',Purple,4,0,10,Apply opponents mana generation to yours.  You may only use this ability once per turn,Silver,10,Increase shields by 5 for your next turn\r\n'
	CSVSources += ',Purple,3,0,5,Switch field colour to any colour,On Colour,5,Leech 1 mana from opponent if their mana is over 40\r\n'
	CSVSources += ',Purple,3,0,5,Ignore all opponent shields.  Lose all your shields their next turn.,Orange,10,Deal 8 damage\r\n'
	CSVSources += ',Purple,3,0,5,Ignore one of your opponent Source\'s shields for your next attack,Off colour,3,Switch field colour to Purple\r\n'
	CSVSources += ',Purple,3,2,15,Calculate the amount of mana you use in your next three attacks.  Destroy half this number of your opponents mana.,n/a,n/a,n/a\r\n'
	CSVSources += ',Purple,3,0,3,Generate 5 mana.  Switch field colour to Silver,Silver,5,Increase shields by 3 for your next turn\r\n'
	CSVSources += ',Purple,3,0,10,Prevent opponent source from switching their field colour next turn,Off colour,3,Attack for 3 damage.  Switch field colour to Purple\r\n'
	CSVSources += ',Purple,2,1,6,On your opponents next turn+ calculate the amount of mana they used.  Add this to your own at the end of their turn.,Silver,5,Increase shields by 3 for your next turn\r\n'
	CSVSources += ',Purple,2,0,2,Reduce opponent mana by 2.  You may only play this abilitiy once per turn.,Blue,5,Detain opponent ability  You may only play this ability once per turn.\r\n'
	CSVSources += ',Purple,2,1,6,Target source does not generate mana during their next turn.,Off colour,1,Generate 3 mana.  You may only play this ability once per turn\r\n'
	CSVSources += ',Purple,1,1,3,Leech 1 mana from opponent if their mana is over 50,Silver,5,Gain 4 knowledge+ increase shield by 2\r\n'
	CSVSources += ',Purple,1,0,5,Apply ignore shield to other Source\'s next attack,Orange,6,Deal 5 damage\r\n'
	CSVSources += ',Silver,3,1,10,Gain 3 knowledge.,Orange,10,Attack for 2 damage.\r\n'
	CSVSources += ',Silver,3,2,3,Change field colour,Off colour,5,Attack for 1 damage.  The damage is dealt if you have at least 4 shields.\r\n'
	CSVSources += ',Silver,3,2,10,Gain 2 knowledge.  If you have less <= 6 shields+ you may use this ability again.,Off colour,5,Deal 1 damage\r\n'
	CSVSources += ',Silver,3,2,10,Kill target opponent Source if their shield is <= 2.  You may not use this ability again this turn.,Blue,10,Detain target source.  Gain 1 knowledge\r\n'
	CSVSources += ',Silver,2,1,5,Gain 4 knowledge.,Silver,10,Gain 2 shield if you have <= 4 shield for the rest of this turn.\r\n'
	CSVSources += ',Silver,2,2,10,Kill target source.  This Source is sarificed.,Orange,10,Deal 3 damage.\r\n'
	CSVSources += ',Silver,2,2,3,Gain 2 knowledge.  Lose 2 shield for this turn.,Off colour,6,Gain 2 shield.  This source is now detained for this turn.\r\n'
	CSVSources += ',Silver,2,2,4,Gain 5 knowledge.  Switch field colour,Off colour,10,Deal 2 damage\r\n'
	CSVSources += ',Silver,2,2,10,Kill target Source.  You may not use this ability again this turn.,Silver,5,Gain 5 knowledge.  Gain an additional 5 if you can deal more than 5 damage to your opponent.\r\n'
	CSVSources += ',Silver,2,1,5,Increase shields by 3 for your opponents next turn,Blue,10,Attack for 3 damage\r\n'
	CSVSources += ',Silver,2,3,3,Switch field colour.  If you generate less than 3 mana currently+ you may use this ability again.,Silver,8,Kill target opponent Source.  YOu may not use this ability again this turn.\r\n'
	CSVSources += ',Silver,2,3,7,Gain 7 knowledge.  Your shield count is hidden from your opponent for the next turn.,Silver,10,Increase shields by 3 for your opponents next turn.\r\n'
	CSVSources += ',Silver,2,3,4,Gain 3 knowledge.  Switch field colour to your choice.,Purple,8,If you have less shield than your opponent+ gain 1/4 of their mana.  If you have more+ gain 1/6.\r\n'
	CSVSources += ',Silver,2,4,8,If your opponent has at least one Source with 0 shield+ gain 10 knowledge.  You may not switch field colour this turn.,n/a,n/a,n/a\r\n'
	CSVSources += ',Silver,2,2,3,Change field colour to any colour but Silver.,Orange,10,Attack for 5 damage\r\n'
	CSVSources += ',Silver,2,2,3,Gain 5 knowledge.  Gain an additional 5 knowledge and drain an additiona 5 mana if opponent changes field colour on their next turn.,Purple,10,Ignore enemy shields\r\n'
	CSVSources += ',Silver,1,0,8,Kill target Source.  Gain 5 knowledge.  Field colour changes to orange.  Field colour cannot change again this turn.,Orange,5,Deal 3 damage.  You may not use this ability again this turn.\r\n'
	CSVSources += ',Silver,1,4,3,Change field colour to any colour but Silver.  You may not use this ability again this turn.,n/a,n/a,n/a\r\n'
	CSVSources += ',Silver,1,2,1,Gain 2 knowledge.  You may not use this ability again this turn.,Orange,15,Deal 10 damage over the next 5 turns.  You turn immedately ends after using this ability.  Cannot be used again until the 5 turns is up.\r\n'
	CSVSources += ',Silver,1,2,3,Gain 5 knowledge.  You may not use this ability again this turn.,Purple,5,Leech 10 mana.  Switch field colour.  Field colour cannot be purple again this turn.\r\n'
	CSVSources += ',Blue,6,0,6,Switch the field color to Purple or Silver.,Off Color,1,If the field is purple+ gain 3 energy. If the field is silver+ gain 1 knowledge. You may only use this ability once per turn.\r\n'
	CSVSources += ',Blue,5,1,8,Switch the field color to any color.,Off Color,10,Switch the field color to Purple.\r\n'
	CSVSources += ',Blue,5,1,8,Detain target source. Gain 1 knowledge.,Off Color,2,Detain target source. Your opponent may also detain target source.\r\n'
	CSVSources += ',Blue,5,0,5,Attack for 2 damage. Switch the field color to Silver.,Purple,4,Destory target source. Sacrafice this source.\r\n'
	CSVSources += ',Blue,5,1,8,Deal 2 damage+ gain 1 knowledge. Switch the field color to orange.,Orange,7,Draw a card. Take 3 damage. Switch the field color to blue.\r\n'
	CSVSources += ',Blue,4,1,10,Your opponent may not change their field color next turn. Your opponent may discard a card to prevent this effect (if they have no cards they cannot prevent the effect).,n/a,n/a,n/a\r\n'
	CSVSources += ',Blue,4,0,8,If you have two sources in play+ both with 0 shields+ gain 10 knowledge. You lose 2 life.,Silver,4,During your opponents next turn+ you gain 3 shields.\r\n'
	CSVSources += ',Blue,4,0,8,Sacrafice this source. Neither opponent may win by acquiring 50 knowledge for the next two turns.,Off Color,8,Sacrafice this source. Neither opponent may win by acquiring 100 mana for the next two turns.\r\n'
	CSVSources += ',Blue,3,0,3,Switch the field color to any color.,Off Color,3,Draw up to 2 cards. Your opponent draws that many cards plus 1.\r\n'
	CSVSources += 'Sly Deal,Blue,3,1,15,You and your opponent each secretly choose to attack for 5 damage+ gain 10 knowledge+ or gain 20 mana. Once you\'ve both chosen+ reveal your choices and apply those effects (yours first). If you both chose the same+ you may choose and apply one of the three effects again.,Blue,8,Gain 10 knowledge. Your opponent may pay 5 life to prevent this.\r\n'
	CSVSources += ',Blue,3,2,0,Switch the field color. Your opponent gains 2 mana.,Blue,0,Switch the field color. Your opponent gains 2 knowledge.\r\n'
	CSVSources += ',Blue,3,1,40,Destroy all your opponents sources+ then attack for 10 damage.,Silver,3,Attack for 3 damage. Switch the field color to Blue.\r\n'
	CSVSources += ',Blue,2,3,8,Sacrafice this source. Detain 2 opponent sources.,Off Color,6,Sacrafice this source. Detain 1 opponent source.\r\n'
	CSVSources += ',Blue,2,0,3,Attack for 2 damage.,Off Color,6,Destroy target source.\r\n'
	CSVSources += ',Blue,2,4,15,Attack for 3 damage.,n/a,n/a,n/a\r\n'
	CSVSources += ',Blue,2,2,5,Attack for 3 damage.,Orange,8,Reduce your opponents energy by 5. You cannot reduce their energy to less than 10 in this way.\r\n'
	CSVSources += ',Blue,1,0,14,Detain two target sources.The sources you detained this way generate at half efficiency+ next turn.,Blue,6,For each detained creature your opponent controls+ you gain 5 knowledge.\r\n'
	CSVSources += ',Blue,1,3,3,During your opponents next turn+ add 1 to your shields.,Silver,4,During your opponents next turn+ add 3 to your shields.\r\n'
	CSVSources += ',Blue,0,2,6,Gain 4 knowledge. Switch the field color to Silver.,Silver,5,Gain 5 knowledge. Switch the field color to Blue.\r\n'
	CSVSources += ',Blue,0,0,4,Discard a card+ draw a card. Pay 1 life.,Off Color,0,Gain 4 energy. You may only use this ability once per turn.\'\r\n'
	
var CSVSpells = ',Blue,,,12,Attack for 5 damage+ gain 8 knowledge+ or gain 27 mana.,Off Color,9,Switch your field color to blue.\r\n';
	CSVSpells +=',Purple,,,15,Target opponent takes 3 damage and loses 5 mana. You gain 4 knowledge.,Off Color,6,Switch your field color to purple.\r\n';
	CSVSpells +=',Silver,,,6,This card does not go to your discard pile when you play this ability. After the beggining of your turn+ reduce your opponent\'s shields by X or you gain X knowledge+ where X is the number of counters on this card. At the end of your opponents turn+ if this card is faceup put a counter on it. On your turn+ if this card has 3 counters on it+ put it into your graveyard.,Off Color,15,Switch your field color to silver.\r\n';
	CSVSpells +=',Orange,,,9,Deal 5 damage to your opponent.,Off Color,12,Switch your field color to orange.\r\n';
	CSVSpells +=',Colorless,,,0,When you activate this card your opponent may choose one: you take 5 damage+ you lose 8 mana+ or they gain 10 knowledge. If your opponent chooses to do nothing draw 1 card+ otherwise draw 2.,n/a,n/a,n/a\r\n';
	CSVSpells +=',Blue,,,10,Counter target ability. Your opponent may switch their field color to any color.,Blue,5,Counter target ability+ its controller may pay the abilities mana cost again to ignore this effect.\r\n';
	CSVSpells +=',Blue,,,20,Counter target ability. Destroy the source that activated it.,n/a,n/a,n/a\r\n';
	CSVSpells +=',Purple,,,15,Kill target source,Purple,10,Ignore all opponent shields this turn\r\n';
	CSVSpells +=',Purple,,,10,Target source does not generate sheild until your opponents next turn.,Blue,15,Detain target source\r\n';
	CSVSpells +=',Purple,,,12,Kill target creature who used an ability this turn.,n/a,n/a,n/a\r\n';
	CSVSpells +=',Silver,,,2,Increase your shields by 5 until end of turn.,Silver,6,Gain 5 knowledge+ switch your field color to blue.\r\n';
	CSVSpells +=',Silver,,,10,Gain 5 knowledge,Purple,10,Increase shields by 4 until the end of turn\r\n';
	CSVSpells +=',Silver,,,10,This card does not go to your discard pile when you play this ability. After the beggining of your turn+ your opponent takes 2 damage if you have over 50 mana.  At the end of your opponents turn+ if they have 10 or less life+ put this card in the graveyard,Blue,3,Prevent your opponent from switching field colours.  If you pay an additional 8 mana+ you may put this card back in your hand.\r\n';
	CSVSpells +=',Silver,,,3,Increase your shields by 10 until end of turn.,n/a,n/a,n/a\r\n';
	CSVSpells +=',Orange,,,12,Deal X damage to your opponent+ where X is 1/2 their current shield+ rounded up.,Orange,15,Burn X mana from your opponent+ where X is 1/2 your life+ rounded down.\r\n';
	CSVSpells +=',Orange,,,8,Destroy target source.  Your opponent may also destroy target source+ unless you choose to pay an additional 10 mana.,Blue,4,You and your opponent must both change your field colors.\r\n';
	CSVSpells +=',Orange,,,5,Burn 5 mana from your opponent.  Switch field colour to any color but orange.,Silver,10,"Increase your shield by 3 until end of turn.\r\n';
	CSVSpells +=',Orange,,,5,Reduce your opponents mana by 15. You cannot reduce their mana to below 15 in this way.,n/a,n/a,n/a\r\n';
	CSVSpells +=',Orange,,,5,Reduce your opponents mana by 15. You cannot reduce their mana to below 15 in this way.,n/a,n/a,n/a\r\n';
	CSVSpells +=',Orange,,,6,Deal 3 damage to your opponent.,Purple,15,Kill target source.\r\n';
	CSVSpells +=',Orange,,,6,Deal 3 damage to your opponent.,Purple,15,Kill target source.\r\n';